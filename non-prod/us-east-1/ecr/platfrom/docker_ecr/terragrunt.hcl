locals {
  # Automatically load environment-level variables
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))

  # Extract out common variables for reuse
  env = local.environment_vars.locals.environment

  tags = {
      project = "prasad"
  }

  policy = <<EOF
  {
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "AllowPull",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage"
            ]
        }
    ]
}
EOF

life_cycle = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire images older than 14 days",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 14
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF

project_names = {
  "platform" = {
    "ecr_prefix" = "prasad"
    "policy_value" = local.policy
    "lifcycle_value" = local.life_cycle
  },
  "development" = {
   "ecr_prefix" = "khyathi"
   "policy_value" = local.policy
   "lifcycle_value" = local.life_cycle
  }
}

}


# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "/Users/prasadmavuluru/terragrunt_ecr/terraform-aws-ecr"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}


# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {
  name                  = local.project_names
  tags                  = local.tags

  }
