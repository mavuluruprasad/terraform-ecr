# Generated by Terragrunt. Sig: nIlQXj57tbuaRZEa
terraform {
  backend "s3" {
    bucket         = "terragrunt-ecr-terraform-state-non-prod-us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
    key            = "non-prod/us-east-1/ecr/platfrom/docker_ecr/terraform.tfstate"
    region         = "us-east-1"
  }
}
