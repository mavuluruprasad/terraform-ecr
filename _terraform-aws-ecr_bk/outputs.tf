output "arn" {
  description = "Full ARN of the repository"
  value       = [for repo_arn in aws_ecr_repository.repo : repo_arn.arn]
}

output "name" {
  description = "The name of the repository."
  value       = [for repo_name in aws_ecr_repository.repo : repo_name.name]
}

output "registry_id" {
  description = "The registry ID where the repository was created."
  value       = [for repo_id in aws_ecr_repository.repo : repo_id.registry_id] 
}

output "repository_url" {
  description = "The URL of the repository (in the form `aws_account_id.dkr.ecr.region.amazonaws.com/repositoryName`)"
  value       = [for repo_url in aws_ecr_repository.repo : repo_url.repository_url] 
}
